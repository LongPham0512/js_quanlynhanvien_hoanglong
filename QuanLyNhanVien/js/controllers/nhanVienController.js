function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luong = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;

  var nhanVien = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luong,
    chucVu,
    gioLam
  );
  return nhanVien;
}

function renderDanhSachNhanVien(dsnv) {
  var contenHTML = "";

  for (var index = 0; index < dsnv.length; index++) {
    var nhanVien = dsnv[index];

    // switch (nhanVien.chucVu) {
    //   case "1":
    //     nhanVien.chucVu = "Sếp";
    //     break;
    //   case "2":
    //     nhanVien.chucVu = "Trưởng Phòng";
    //     break;
    //   case "3":
    //     nhanVien.chucVu = "Nhân Viên";
    //     break;
    // }

    var contentTr = `<tr>
        <td>${nhanVien.taiKhoan}</td>
        <td>${nhanVien.hoTen}</td>
        <td>${nhanVien.email}</td>
        <td>${nhanVien.ngayLam}</td>
        <td>${nhanVien.chucVu}</td>
        <td>${nhanVien.luong}</td>
        <td>${nhanVien.gioLam}</td>
        <td>${nhanVien.tinhTongLuong()}</td>
        <td>${nhanVien.xepLoaiNV()}</td>
        <td>
            <button data-toggle="modal"
            data-target="#myModal" onclick="suaNhanVien('${
              nhanVien.taiKhoan
            }')" class="btn btn-warning">EDIT</button>

            <button onclick="xoaNhanVien('${
              nhanVien.taiKhoan
            }')" class="btn btn-danger">DELETE</button>
        </td>
    </tr>`;

    contenHTML += contentTr;
  }

  document.getElementById("tableDanhSach").innerHTML = contenHTML;
}
function xuatThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
