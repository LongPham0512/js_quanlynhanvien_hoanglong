function ValidatorNV() {
  this.kiemTraRong = function (idTarget, idErr, messageErr) {
    var valueInput = document.getElementById(idTarget).value.trim();

    if (valueInput == "") {
      document.getElementById(idErr).innerText = messageErr;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };

  this.kiemTraTrung = function (newNhanVien, danhSachNhanVien) {
    var index = danhSachNhanVien.findIndex((item) => {
      return item.taiKhoan == newNhanVien.taiKhoan;
    });

    if (index == -1) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    } else {
      document.getElementById("tbTKNV").innerText = "Tài Khoản đã được sử dụng";
      return false;
    }
  };

  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;

    let valueInput = document.getElementById(idTarget).value;

    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Email khum hợp lệ";
  };

  this.kiemTraMatKhau = function (idTarget, idError) {
    let parten =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;

    let valueInput = document.getElementById(idTarget).value;

    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Mật Khẩu Không Hợp lệ";
  };

  this.kiemTraLuong = (idTarget, idError) => {
    var valueInput = document.getElementById(idTarget).value;

    if (valueInput >= 1e6 && valueInput <= 20e6) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Lương phải từ 1tr đến 20tr";
    }
  };

  this.kiemTraGioLam = (idTarget, idError) => {
    var valueInput = document.getElementById(idTarget).value;

    if (valueInput >= 80 && valueInput <= 200) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Giờ làm phải từ 80h - 200h/tháng";
    }
  };

  this.kiemTraSelections = function (value, idError, messageError) {
    var valueTarget = document.getElementById(value);

    if (valueTarget.selectedIndex == "") {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
}

var validatorNV = new ValidatorNV();
function isValidAll() {
  var newNhanVien = layThongTinTuForm();

  isValidTaiKhoan =
    validatorNV.kiemTraRong("tknv", "tbTKNV", "Tài khoản khum được rỗng") &&
    validatorNV.kiemTraTrung(newNhanVien, danhSachNhanVien);

  isValidHoTen = validatorNV.kiemTraRong(
    "name",
    "tbTen",
    "Họ Tên khum được rỗng"
  );

  isValidEmail =
    validatorNV.kiemTraRong("email", "tbEmail", "Email khum được rỗng") &&
    validatorNV.kiemTraEmail("email", "tbEmail");

  isValidMatKhau =
    validatorNV.kiemTraRong(
      "password",
      "tbMatKhau",
      "Mật khẩu khum được rỗng"
    ) && validatorNV.kiemTraMatKhau("password", "tbMatKhau");

  isValidNgayLam = validatorNV.kiemTraRong(
    "datepicker",
    "tbNgay",
    "Vui lòng điền ngày làm"
  );

  isValidLuong =
    validatorNV.kiemTraRong(
      "luongCB",
      "tbLuongCB",
      "Thích bao nhiêu thì điền"
    ) && validatorNV.kiemTraLuong("luongCB", "tbLuongCB");

  isValidChucVu = validatorNV.kiemTraSelections(
    "chucvu",
    "tbChucVu",
    "Chọn chức vụ"
  );

  isValidGioLam =
    validatorNV.kiemTraRong("gioLam", "tbGiolam", "Vui lòng khum để rỗng") &&
    validatorNV.kiemTraGioLam("gioLam", "tbGiolam");

  return (
    isValidTaiKhoan &&
    isValidHoTen &&
    isValidEmail &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  );
}

function isValidAllCapNhat() {
  isValidTaiKhoan = validatorNV.kiemTraRong(
    "tknv",
    "tbTKNV",
    "Tài khoản khum được rỗng"
  );

  isValidHoTen = validatorNV.kiemTraRong(
    "name",
    "tbTen",
    "Họ Tên khum được rỗng"
  );

  isValidEmail =
    validatorNV.kiemTraRong("email", "tbEmail", "Email khum được rỗng") &&
    validatorNV.kiemTraEmail("email", "tbEmail");

  isValidMatKhau =
    validatorNV.kiemTraRong(
      "password",
      "tbMatKhau",
      "Mật khẩu khum được rỗng"
    ) && validatorNV.kiemTraMatKhau("password", "tbMatKhau");

  isValidNgayLam = validatorNV.kiemTraRong(
    "datepicker",
    "tbNgay",
    "Vui lòng điền ngày làm"
  );

  isValidLuong =
    validatorNV.kiemTraRong(
      "luongCB",
      "tbLuongCB",
      "Thích bao nhiêu thì điền"
    ) && validatorNV.kiemTraLuong("luongCB", "tbLuongCB");

  isValidChucVu = validatorNV.kiemTraSelections(
    "chucvu",
    "tbChucVu",
    "Chọn chức vụ"
  );

  isValidGioLam =
    validatorNV.kiemTraRong("gioLam", "tbGiolam", "Vui lòng khum để rỗng") &&
    validatorNV.kiemTraGioLam("gioLam", "tbGiolam");

  return (
    isValidTaiKhoan &&
    isValidHoTen &&
    isValidEmail &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  );
}
