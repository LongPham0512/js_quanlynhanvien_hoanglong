var danhSachNhanVien = [];

const timKimViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
};

//Lưu LOCAL-STORAGE
const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
const luuLocalStorage = () => {
  var dsnvJson = JSON.stringify(danhSachNhanVien);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
};
//lấy dữ liệu từ localstorage khi user tải lại trang
var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
// Gán cho array gôc và render lại giao diện
if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);
  danhSachNhanVien = danhSachNhanVien.map((item) => {
    return new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luong,
      item.chucVu,
      item.gioLam
    );
  });
  renderDanhSachNhanVien(danhSachNhanVien);
}

//reset lại modal thêm nhân viên
document.getElementById("btnThem").addEventListener("click", () => {
  document.getElementById("form_NV").reset();
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
});

//THÊM NHÂN VIÊN
function themNhanVien() {
  var newNhanVien = layThongTinTuForm();

  var isValid = isValidAll();

  if (isValid) {
    danhSachNhanVien.push(newNhanVien);
    renderDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();
    $("#myModal").modal("hide");
  }
}

//XÓA NHÂN VIÊN
function xoaNhanVien(id) {
  var viTri = timKimViTri(id, danhSachNhanVien);

  danhSachNhanVien.splice(viTri, 1);
  renderDanhSachNhanVien(danhSachNhanVien);
  luuLocalStorage();
}

//SỬA NHÂN VIÊN
function suaNhanVien(id) {
  document.getElementById("btnCapNhat").style.display = "block";
  document.getElementById("btnThemNV").style.display = "none";
  var viTri = timKimViTri(id, danhSachNhanVien);
  var nhanVien = danhSachNhanVien[viTri];

  xuatThongTinLenForm(nhanVien);
}

//CẬP NHẬT NHÂN VIÊN
function capNhatNhanVien() {
  var nhanVienEdit = layThongTinTuForm();
  let viTri = timKimViTri(nhanVienEdit.taiKhoan, danhSachNhanVien);
  danhSachNhanVien[viTri] = nhanVienEdit;

  var isValid = isValidAllCapNhat();
  if (isValid) {
    renderDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();
    $("#myModal").modal("hide");
  }
}

//TÌM KIẾM NHÂN VIÊN THEO XẾP LOẠI
function timNhanVien() {
  var danhSachNhanVienCanTim = [];
  var sinhVienCanTim = document.getElementById("searchName").value.trim();

  danhSachNhanVien.forEach((item) => {
    if (item.xepLoaiNV() == sinhVienCanTim) {
      danhSachNhanVienCanTim.push(item);
    }
    renderDanhSachNhanVien(danhSachNhanVienCanTim);
  });
}

//HIỂN THỊ DANH SÁCH NHÂN VIÊN KHI XÓA SEARCH
function hienThiLaiDSNV() {
  var searchName = document.getElementById("searchName").value;
  if (searchName == "") {
    renderDanhSachNhanVien(danhSachNhanVien);
  }
}
