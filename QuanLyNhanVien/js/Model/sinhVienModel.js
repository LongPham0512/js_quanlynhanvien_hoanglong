var NhanVien = function (
  _taiKhoan,
  _hoTen,
  _email,
  _matKhau,
  _ngayLam,
  _luong,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luong = _luong;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;

  this.tinhTongLuong = () => {
    if (this.chucVu == "Sếp") {
      return this.luong * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luong * 2;
    } else return this.luong * 1;
  };

  this.xepLoaiNV = () => {
    if (this.gioLam >= 192) {
      return "xuất sắc";
    } else if (this.gioLam >= 176) {
      return "giỏi";
    } else if (this.gioLam >= 160) {
      return "khá";
    } else return "trung bình";
  };
};
